
//
//  RatingsViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 12/11/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//


import UIKit
import AssetsLibrary
import Foundation
import Alamofire

class RatingsViewController: RootBaseViewController, UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ratingsDelegate {
    
    @IBOutlet var Review_title: UILabel!
    
    @IBOutlet weak var titleHeader: UILabel!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet var Provider_image: UIImageView!
    
    @IBOutlet var addservicebtn: UIButton!
    
    @IBOutlet weak var provider_Name: CustomLabelGray!
    @IBOutlet var service_image: UIImageView!
     var jobIDStr:String = ""
    @IBOutlet weak var ratingBtn: UIButton!
    let imagePicker = UIImagePickerController()
    //var theme:Theme=Theme()
    var get_imagedata : Data = Data()
    var get_pickerimage: UIImage?

    var ratingsOptArr:NSMutableArray = [];
    @IBOutlet weak var reviewTxtView: UITextView!
    @IBOutlet weak var reviewTblView: UITableView!
    override func viewDidLoad() {
        self.ratingBtn.isHidden=true
        super.viewDidLoad()
        
        
        btnSkip.setTitle(Language_handler.VJLocalizedString("skip", comment: nil), for: UIControlState())
        titleHeader.text = Language_handler.VJLocalizedString("rating", comment: nil)
        ratingBtn.setTitle(Language_handler.VJLocalizedString("submit", comment: nil), for: UIControlState())
        addservicebtn.setTitle(Language_handler.VJLocalizedString("add_image", comment: nil), for: UIControlState())
        
        Provider_image.layer.cornerRadius=Provider_image.frame.size.width/2
        Provider_image.clipsToBounds=true
       // Provider_image.layer.borderWidth=1.0
       // Provider_image.layer.borderColor = theme.Lightgray().cgColor
        

            imagePicker.delegate=self
        reviewTxtView.layer.cornerRadius=5
        reviewTxtView.layer.borderWidth=1
        reviewTxtView.text = theme.setLang("Review")
        //reviewTxtView.layer.borderColor=PlumberLightGrayColor.cgColor
        reviewTxtView.layer.masksToBounds=true
        reviewTblView.register(UINib(nibName: "RatingsTableViewCell", bundle: nil), forCellReuseIdentifier: "RatingCellIdentifier")
        reviewTblView.estimatedRowHeight = 125
        reviewTblView.rowHeight = UITableViewAutomaticDimension
        reviewTblView.tableFooterView = UIView()
        GetRatingsOption()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didclickimage(_ sender: AnyObject) {
        let ImagePicker_Sheet = UIAlertController(title: nil, message: "\(Language_handler.VJLocalizedString("select_image", comment: nil))", preferredStyle: .actionSheet)
        
        let Camera_Picker = UIAlertAction(title: "\(Language_handler.VJLocalizedString("camera", comment: nil))", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.Camera_Pick()
        })
        let Gallery_Picker = UIAlertAction(title: "\(Language_handler.VJLocalizedString("gallery", comment: nil))", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //
            self.Gallery_Pick()
            
        })
        
        let cancelAction = UIAlertAction(title: "\(Language_handler.VJLocalizedString("cancel", comment: nil))", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        ImagePicker_Sheet.addAction(Camera_Picker)
        ImagePicker_Sheet.addAction(Gallery_Picker)
        ImagePicker_Sheet.addAction(cancelAction)
        
        self.present(ImagePicker_Sheet, animated: true, completion: nil)
        
    }
    
    func Camera_Pick()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .camera
            self.imagePicker.modalPresentationStyle = .popover
            self.present(self.imagePicker, animated: true, completion: nil)
        }
            
        else
        {
            Gallery_Pick()
        }
    }
    
    func Gallery_Pick()
    {
        
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.modalPresentationStyle = .popover
        self.present(self.imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //handle media here i.e. do stuff with photo
        
        picker.dismiss(animated: true, completion: nil)
        let url = info[UIImagePickerControllerReferenceURL]
        
        addservicebtn.setTitle("", for:UIControlState())
        

        if (url !=  nil)
        {
            
            let pickimage = self.theme.removeRotation(info[UIImagePickerControllerOriginalImage] as! UIImage)
           get_pickerimage =  UIImage.init(cgImage: pickimage.cgImage!, scale: 0.25 , orientation:.downMirrored)
            
            get_imagedata = UIImageJPEGRepresentation(get_pickerimage! ,0.1)!
            
            service_image .image = info[UIImagePickerControllerOriginalImage] as! UIImage
            //data!.writeToFile(localPath, atomically: true)
            
        }
        else
        {
            
            let pickimage = self.theme.removeRotation(info[UIImagePickerControllerOriginalImage] as! UIImage)
           get_pickerimage =  UIImage.init(cgImage: pickimage.cgImage!, scale: 0.25 , orientation:.downMirrored)
            
            get_imagedata = UIImageJPEGRepresentation(get_pickerimage! ,0.1)!
            
            service_image .image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            
        }
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //what happens when you cancel
        //which, in our case, is just to get rid of the photo picker which pops up
        picker.dismiss(animated: true, completion: nil)
    }

    
    func GetRatingsOption(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["holder_type":"provider", "user":
            objUserRecs.providerId,"job_id":jobIDStr] as [String : Any]
       
        self.showProgress()
        url_handler.makeCall(GetRatingsOptionUrl, param: Param  as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    if(status == "1")
                    {
                        let  listArr:NSArray=(responseObject.object(forKey: "review_options") as? NSArray)!
                        self.Review_title.text = self.theme.CheckNullValue((listArr[0] as AnyObject).object(forKey: "option_name"))!
                        if(listArr.count>0){
                             self.ratingBtn.isHidden=false
                            
                            for (_, element) in listArr.enumerated() {
                                let result1:RatingsRecord=RatingsRecord()
                                result1.title=self.theme.CheckNullValue((element as AnyObject).object(forKey: "option_title"))!
                                let optionInt : Int = ((element as AnyObject).object(forKey: "option_id")) as! Int
                                let strOptionId = "\(optionInt)"
                                result1.optionId=self.theme.CheckNullValue(strOptionId)!
                                self.provider_Name.text = self.theme.CheckNullValue((element as AnyObject).object(forKey: "username"))!
                                self.Provider_image.sd_setImage(with: URL(string:self.theme.CheckNullValue((element as AnyObject).object(forKey: "image"))!), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                                self.Review_title.text = self.theme.CheckNullValue((element as AnyObject).object(forKey: "option_name"))!

                                //result1.optionId=self.theme.CheckNullValue(element.objectForKey("option_id"))!
                                result1.rateCount = String(0)
                                self.ratingsOptArr .add(result1)
                            }
                            
                            
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                        }
                        self.reviewTblView.reload()
                        //This code will run in the main thread:
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    
    func SaveUserRatings(){
       
        
            
                
                let param : NSDictionary =  self.dictForrating()
                
                 NSLog("getDevicetoken =%@", self.dictForrating())
                self.showProgress()
                let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let URL = try! URLRequest(url: SaveRatingsOptionUrl, method: .post, headers: ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(objUserRecs.providerId)"])
            
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(self.get_imagedata, withName: "file", fileName: "file.png", mimeType: "")
            
            for (key, value) in param {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
            
        }, with: URL, encodingCompletion: { encodingResult in
            
                switch encodingResult {
                
                case .success(let upload, _, _):
                print("s")
                
                upload.responseJSON { response in
                
                
                if let J = response.result.value {
                
                    let JSON = J as! NSDictionary
                self.DismissProgress()
                print("JSON: \(JSON)")
                let Status = self.theme.CheckNullValue(JSON.object(forKey: "status"))!
                let response = JSON.object(forKey: "response") as! NSDictionary
                if(Status == "1")
                {
                
                
                //        self.view.makeToast(message:self.themes.CheckNullValue(response.objectForKey("msg"))!, duration: 4, position: HRToastPositionDefault as AnyObject, title: "")
                
                    self.navigationController?.popToRootViewController(animated: true)
                
                }
                else
                {
                
                //  self.themes.AlertView("Image Upload Failed", Message: "Please try again", ButtonTitle: "Ok")
                    self.view.makeToast(message:self.theme.CheckNullValue(response.object(forKey: "msg"))!, duration: 4, position: HRToastPositionDefault as AnyObject, title: "")
                
                
                }
                
                
                
                
                
                
                }
                }
                
                case .failure(let encodingError):
                self.DismissProgress()
                print(" the encodeing error is \(encodingError)")
                
                self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ratingsOptArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->     UITableViewCell {
        let cell:RatingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RatingCellIdentifier") as! RatingsTableViewCell
        cell.objIndexPath=indexPath
        cell.loadRateTableCell(ratingsOptArr.object(at: indexPath.row) as! RatingsRecord)
        cell.delegate=self
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        return cell
    }
    @IBAction func didClickBackbtn(_ sender: AnyObject) {
    
         self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func didClickRateUserBtn(_ sender: AnyObject) {
        self.theme.AlertView("\(appNameJJ)", Message:"\(theme.setLang("Your Rating Submitted Successfully"))", ButtonTitle: kOk)
        
                SaveUserRatings()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dictForrating()->NSDictionary{
        var cmtstr:NSString=""
        if(reviewTxtView.text=="Review"){
            cmtstr=""
        }else{
            cmtstr=reviewTxtView.text as! NSString
        }
        
        let reviewDict:NSMutableDictionary=NSMutableDictionary()
        for i in 0 ..< ratingsOptArr.count {
            let str1: String = "ratings[\(i)][option_title]"
            let str2: String = "ratings[\(i)][option_id]"
            let str3: String = "ratings[\(i)][rating]"
            let objRatingsRecs: RatingsRecord = ratingsOptArr[i] as! RatingsRecord
            reviewDict.setValue(objRatingsRecs.title, forKey:str1)
            reviewDict.setValue(objRatingsRecs.optionId,forKey:str2)
            reviewDict.setValue(objRatingsRecs.rateCount, forKey: str3)
        }
        reviewDict.setValue("tasker", forKey: "ratingsFor")
        reviewDict.setValue(jobIDStr, forKey: "job_id")
        reviewDict.setValue(cmtstr, forKey: "comments")
        reviewDict.setValue("ios", forKey: "type") 

        

        return reviewDict
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(range.location==0 && text==" "){
            return false
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    func ratingsCount(_ withRateVal:Float , withIndex:IndexPath){
        
        let objRatingsRecs: RatingsRecord = ratingsOptArr[withIndex.row] as! RatingsRecord
        objRatingsRecs.rateCount = String (withRateVal)
        ratingsOptArr.replaceObject(at: withIndex.row, with: objRatingsRecs)
       
        self.reviewTblView.beginUpdates()
        self.reviewTblView.reloadRows(at: [withIndex], with: .none)
        self.reviewTblView.endUpdates()
    }
    func validateTxtFields () -> Bool{
    var isOK:Bool=true
        if(reviewTxtView.text.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("review_mand", comment: nil))
            isOK = false
            return isOK
        }
//        for var i = 0; i < ratingsOptArr.count; i++ {
//            let objRatingsRecs: RatingsRecord = ratingsOptArr[i] as! RatingsRecord
//            let rateVal: Float = Float(objRatingsRecs.rateCount as String)!
//            if rateVal == 0 {
//                ValidationAlert("\(objRatingsRecs.title) is mandatory")
//                isOK = false
//                 break;
//            }
//        }
        
        
        
        
        
        
        
        
    return isOK
    }
    
    func ValidationAlert(_ alertMsg:String){
        let popup = NotificationAlertView.popupWithText("\(alertMsg)")
        popup.hideAfterDelay = 3
        //popup.position = NotificationAlertViewPosition.Bottom
        popup.animationDuration = 1
        popup.show()
    }
    
 
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
