//
//  MyProfileViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/6/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import DGElasticPullToRefresh

let IMAGE_HEIGHT = 273
class MyProfileViewController: RootBaseViewController,SMSegmentViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate {
    //   var theme:Theme=Theme()
    var availabilityDict : NSMutableArray = NSMutableArray()
    var AvailableDaysArray :NSMutableArray = NSMutableArray()
    var AvailableAllDaysArray :NSMutableArray = NSMutableArray()
   // var categoryArray:NSMutableArray = NSMutableArray()
    var weekFullDay = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

    var nextPageStr:NSInteger!
    var noDataView:UIView!
    var segmentView: SMSegmentView!
    
    var getCatagoryArr : NSArray = NSArray()
    @IBOutlet var profileseegment: CustomSegmentControl!

    @IBOutlet weak var title_lbl: CustomLabelHeader!
    @IBOutlet var availabletable: UITableView!
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topView: SetColorView!
    @IBOutlet weak var segmentContainerView: UIView!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var totalContainer: UIView!
    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var reviewsTblView: UITableView!
    @IBOutlet weak var userInfoTopView: UIView!
    @IBOutlet weak var myProfileTblView: UITableView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var userCatLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    var ProfileContentArray:NSMutableArray = [];
    var reviewsArray:NSMutableArray = [];
    var catagoryarrcount : NSMutableArray = [];
    
    @IBOutlet weak var profileTopView: UIView!
    fileprivate var loading = false {
        didSet {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editProfileBtn.setTitle(Language_handler.VJLocalizedString("edit_profile", comment: nil), for: UIControlState())
        self.title_lbl.text = theme.setLang("my_profile")
        
        
        profileseegment.setTitle(theme.setLang("detail"), forSegmentAt: 0)
        profileseegment.setTitle(theme.setLang("availability"), forSegmentAt: 1)
        profileseegment.selectedSegmentIndex=0
        profileseegment.tintColor=theme.additionalThemeColour()
        profileseegment.isHidden = true
        
        profileseegment.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 14.0)!, NSForegroundColorAttributeName: PlumberThemeColor], for: UIControlState())

        
        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.isHidden=false;
            barButton.isHidden=true
        }else{
            
        }
        myProfileTblView.register(UINib(nibName: "ProfileDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailTableIdentifier")
        myProfileTblView.estimatedRowHeight = 58
        myProfileTblView.rowHeight = UITableViewAutomaticDimension
        
        availabletable.register(UINib(nibName:"AvailableDaysTableCell", bundle: nil), forCellReuseIdentifier: "availabledayscell")
        availabletable.estimatedRowHeight = 20
        availabletable.rowHeight = UITableViewAutomaticDimension
        
        
        barButton.addTarget(self, action: #selector(MyProfileViewController.openmenu), for: .touchUpInside)
        reviewsTblView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTblIdentifier")
        reviewsTblView.estimatedRowHeight = 120
        reviewsTblView.rowHeight = UITableViewAutomaticDimension
        myProfileTblView.tableFooterView = UIView()
        availabletable.tableFooterView = UIView()
        reviewsTblView.tableFooterView = UIView()
        blurBannerImg()
        loadSegmentControl()
        
    }
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    
    
    
    @IBAction func segmentAction(_ sender: AnyObject) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;

        if(segmentIndex == 0)
        {
            self.availabletable.isHidden=true
            self.myProfileTblView.isHidden=false
        }
        if(segmentIndex == 1)
        {
            
            self.availabletable.isHidden=false
            self.myProfileTblView.isHidden=true
            
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        reviewsTblView.isHidden=true
        nextPageStr=1
        if(ProfileContentArray.count>0){
            ProfileContentArray.removeAllObjects()
            self.availabilityDict.removeAllObjects()
            self.AvailableDaysArray.removeAllObjects()
            self.reviewsArray.removeAllObjects()
            self.catagoryarrcount.removeAllObjects()
        }
        
        refreshNewLeads()
        showProgress()
        
        GetReviews()
        GetUserDetails()
    }
    func loadProfileTblView(){
        
        
        self.profileScrollView.frame=CGRect(x: self.profileScrollView.frame.origin.x, y: self.profileScrollView.frame.origin.y, width: self.profileScrollView.frame.size.width, height: self.profileScrollView.frame.size.height);
        
        self.myProfileTblView.frame = CGRect(x: self.myProfileTblView.frame.origin.x, y: self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, width: self.myProfileTblView.frame.size.width, height: self.myProfileTblView.frame.size.height+self.myProfileTblView.contentSize.height+CGFloat(getCatagoryArr.count*5))
        self.availabletable.frame = CGRect(x: self.availabletable.frame.origin.x, y: self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, width: self.availabletable.frame.size.width, height: CGFloat(AvailableDaysArray.count*56))
        self.profileScrollView.contentSize=CGSize( width: self.profileScrollView.frame.width,height: self.myProfileTblView.frame.origin.y + self.myProfileTblView.contentSize.height+50)
        
        self.myProfileTblView.isHidden = false
        self.availabletable.isHidden = true
        print(self.profileScrollView.frame);
        print(self.myProfileTblView.frame);
        print(self.availabletable.frame);
        print(self.profileScrollView.contentSize);
        
        
    }
    
    
    
    func blurBannerImg(){
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            bannerImg.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bannerImg.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            bannerImg.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        }
        else {
            bannerImg.backgroundColor = UIColor.black
        }
        editProfileBtn.backgroundColor=PlumberThemeColor
        userImg.layer.cornerRadius=userImg.frame.size.width/2
        //userImg.layer.borderWidth=0.75
        //userImg.layer.borderColor=PlumberThemeColor.cgColor
        userImg.layer.masksToBounds=true
    }
    func loadSegmentControl(){
        self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: segmentContainerView.frame.size.width, height: segmentContainerView.frame.size.height), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5, segmentProperties: [keySegmentTitleFont: UIFont.boldSystemFont(ofSize: 15.0), keySegmentOnSelectionColour: PlumberThemeColor,keySegmentOffSelectionTextColour:UIColor.darkGray, keySegmentOffSelectionColour: UIColor.white, keyContentVerticalMargin: Float(10.0) as AnyObject])
        
        self.segmentView.delegate = self
        
        self.segmentView.layer.cornerRadius = 0.0
        self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).cgColor
        self.segmentView.layer.borderWidth = 1.0
        
        // Add segments
        self.segmentView.addSegmentWithTitle(title: Language_handler.VJLocalizedString("profiles", comment: nil), onSelectionImage: UIImage(named: "MyProfileSelect"), offSelectionImage: UIImage(named: "MyProfileUnSelect"))
        self.segmentView.addSegmentWithTitle(title: Language_handler.VJLocalizedString("reviews", comment: nil), onSelectionImage: UIImage(named: "ReviewsSelect"), offSelectionImage: UIImage(named: "ReviewsUnSelect"))
        
        
        // Set segment with index 0 as selected by default
        //segmentView.selectSegmentAtIndex(0)
        self.segmentView.selectSegmentAtIndex(index: 0)
        segmentContainerView.addSubview(self.segmentView)
    }
    // SMSegment Delegate
    func segmentView( segmentView: SMSegmentView, didSelectSegmentAtIndex index: Int) {
        switch (index){
        case 0:
            swapViews(false)
            break
        case 1:
            swapViews(true)
            break
            
        default:
            break
        }
    }
    func swapViews(_ isReviews:Bool){
        
        let transition = CATransition()
        // transition.startProgress = 0;
        //transition.endProgress = 5.0;
        transition.type = "fade";
        //transition.subtype = "fromRight";
        transition.duration = 0.4;
        transition.repeatCount = 1;
        self.totalContainer.layer.add(transition, forKey: " ")
        if(isReviews==true){
            profileView.isHidden=true
            reviewsView.isHidden=false
            
            if self.reviewsArray.count == 0
            {
                
                self.view.makeToast(message:Language_handler.VJLocalizedString("not_received_yet", comment: nil) , duration: 2, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                
            }
            
        }else{
            profileView.isHidden=false
            reviewsView.isHidden=true
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView==profileScrollView){
            let offset: CGFloat = scrollView.contentOffset.y
            let percentage: CGFloat = (offset / CGFloat(IMAGE_HEIGHT))
            let value: CGFloat = CGFloat(IMAGE_HEIGHT) * percentage
            bannerImg.frame = CGRect(x: 0, y: value, width: bannerImg.bounds.size.width, height: CGFloat(IMAGE_HEIGHT) - value)
            let alphaValue: CGFloat = 1 - fabs(percentage)
            userInfoTopView.alpha = alphaValue * alphaValue * alphaValue
            
        }
    }
    @IBAction func didClickEditProfile(_ sender: AnyObject) {
        let objEditProfVc = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileVCSID") as! EditProfileViewController
        objEditProfVc.availabilityStorage = self.AvailableAllDaysArray
      //  objEditProfVc.category_Array = categoryArray
        self.navigationController!.pushViewController(withFlip: objEditProfVc, animated: true)
    }
    @IBAction func didClickBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {   let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 20))
        
        if tableView .isEqual(availabletable)
        {
            
            let headerlable : UILabel = UILabel.init(frame:CGRect(x: headerView.frame.origin.x+10,y: 10,width: headerView.frame.size.width-10,height: 25))
            if (AvailableDaysArray.count > 0)
            {
                headerlable.text = Language_handler.VJLocalizedString("available_days", comment: nil)            }
            headerlable.textColor = UIColor(red: 255/255.0, green: 70/255.0, blue: 63/255.0, alpha: 1)
            headerlable.font = UIFont.init(name: "Roboto-Regular", size:14.0)
            
            headerView.addSubview(headerlable)
            
            return headerView
        }
        else
        {
            return headerView
        }
    }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView==myProfileTblView){
            
            return ProfileContentArray.count
        }else if(tableView==reviewsTblView){
            return reviewsArray.count
        }
        else if (tableView ==  availabletable)
        {
            
            return AvailableDaysArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->     UITableViewCell {
        
        let cell3:UITableViewCell
        
        if(tableView==myProfileTblView){
            
            
            
            
            let cell1:ProfileDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailTableIdentifier") as! ProfileDetailTableViewCell
            if ProfileContentArray.count > 0
            {
                cell1.loadProfileTableCell(ProfileContentArray .object(at: indexPath.row) as! ProfileContentRecord)
            }
            cell1.selectionStyle=UITableViewCellSelectionStyle.none
            cell3=cell1
            
        }
        else if (tableView == availabletable)
        {
            let avialCell:AvailableDaysTableCell = tableView.dequeueReusableCell(withIdentifier: "availabledayscell") as! AvailableDaysTableCell
            
            if indexPath.row == 0
            {
                avialCell.Morning.isHidden = false
                avialCell.afternoon.isHidden = false
                avialCell.Evning.isHidden = false
                avialCell.mrnbtn.isHidden = true
                avialCell.afternbtn.isHidden = true
                avialCell.evebtn.isHidden = true
            }
            else
            {
                avialCell.Morning.isHidden = true
                avialCell.afternoon.isHidden = true
                avialCell.Evning.isHidden = true
                avialCell.mrnbtn.isHidden = false
                avialCell.afternbtn.isHidden = false
                avialCell.evebtn.isHidden = false
            }
            
            
            if AvailableDaysArray.count > 0
            {
                avialCell.loadProfileTableCell(self.AvailableDaysArray .object(at: indexPath.row) as! AvailableRecord)
                
            }
            avialCell.selectionStyle=UITableViewCellSelectionStyle.none
            
            cell3=avialCell
            
            
        }
        else{
            let cell:ReviewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTblIdentifier") as! ReviewsTableViewCell
            
            if (reviewsArray.count > 0)
            {
                cell.loadReviewTableCell((reviewsArray .object(at: indexPath.row) as! ReviewRecords), currentView:MyProfileViewController() as UIViewController)
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyle.none
            cell3=cell
        }
        
        return cell3
    }
    
    //    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    //        if(tableView==myProfileTblView){
    //        let cellSize:CGRect = cell.frame;
    //            if(indexPath.row+1==ProfileContentArray.count){
    //                var frame: CGRect = self.myProfileTblView.frame;
    //                frame.size.height = cellSize.origin.y + cellSize.height+10;
    //                self.myProfileTblView.frame = frame;
    ////                 self.profileScrollView.contentSize=CGSizeMake(self.profileScrollView.frame.size.width, self.myProfileTblView.frame.origin.y+self.myProfileTblView.frame.size.height)
    //            }
    //        }
    //    }
    func GetUserDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(viewProfile, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    
                    if(status == "1")
                    {
                        
                        self.profileseegment.isHidden = false
                        self.ProfileContentArray.removeAllObjects()
                        self.availabilityDict.removeAllObjects()
                        self.AvailableDaysArray.removeAllObjects()
                        self.catagoryarrcount.removeAllObjects()
                        // self.categoryArray.removeAllObjects()
                        
                        self.profileTopView.isHidden=false
                        self.userNameLbl.text=self.theme.CheckNullValue((responseObject.object(forKey: "response") as AnyObject).object(forKey: "provider_name") as AnyObject)
                        
                        self.ratingView.rating =  Float((responseObject.object(forKey: "response") as AnyObject).object(forKey: "avg_review") as! NSString as String)!
                        let Dict : NSDictionary = (responseObject.object(forKey: "response"))! as! NSDictionary
                        self.userImg.sd_setImage(with: URL(string:(Dict.object(forKey: "image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                        
                        self.theme.saveUserImage(self.theme.CheckNullValue(Dict.object(forKey: "image")! as AnyObject)! as NSString)
                        
                        
                        self.bannerImg.sd_setImage(with: URL(string:((responseObject.object(forKey: "response") as AnyObject).object(forKey: "image"))as! String), placeholderImage: UIImage(named: "PlaceHolderBig"))
                        
                        if(((responseObject.object(forKey: "response") as AnyObject).object(forKey: "details")! as AnyObject).count>0){
                            let  listArr:NSArray=(responseObject.object(forKey: "response") as AnyObject).object(forKey: "details") as! NSArray
                            
                            self.availabilityDict = (responseObject.object(forKey: "response") as AnyObject).object(forKey: "availability_days") as! NSMutableArray
                            
                            let categoryArr = (responseObject.object(forKey: "response") as AnyObject).object(forKey: "category_details") as! NSArray
                            
//                            for category_Arr in categoryArr{
//                                let id = self.theme.CheckNullValue(category_Arr["_id"])
//                                let name = self.theme.CheckNullValue(category_Arr["name"])
////                                self.categoryArray.addObject(CategoryList.init(categoryName:name!,category_ID:id!))
//                                //
//                            }
                            for (_, element) in listArr.enumerated() {
                                if   self.theme.CheckNullValue((element as AnyObject).object(forKey: "desc") as AnyObject)! == "" || self.theme.CheckNullValue((element as AnyObject).object(forKey: "title") as AnyObject) == "Bio"
                                {
                                    
                                    print("remove bio field")
                                }
                                else{
                                    
                                    let result1 = self.theme.CheckNullValue((element as AnyObject).object(forKey: "desc") as AnyObject)!.replacingOccurrences(of: "\n", with:",")
                                    
                                    let rec = ProfileContentRecord(userTitle: self.theme.CheckNullValue((element as AnyObject).object(forKey: "title") as AnyObject)!, desc: result1)
                                    
                                    
                                    if self.theme.CheckNullValue((element as AnyObject).object(forKey: "title") as AnyObject)! == "Category"
                                    {
                                        
                                        self.getCatagoryArr = result1.components(separatedBy: ",") as NSArray
                                        
                                    }
                                    
                                    
                                    //
                                    //
                                    
                                    print("category array count \(self.getCatagoryArr.count)")
                                    self.ProfileContentArray .add(rec)
                                    
                                    
                                }
                                
                                
                            }
                          let record  = AvailableRecord (dayrec: Language_handler.VJLocalizedString("days", comment: nil)
                                ,mornigrec:Language_handler.VJLocalizedString("morning", comment: nil)
                                ,Afterrec:Language_handler.VJLocalizedString("afternoon", comment: nil)
                                ,eveningrec:Language_handler.VJLocalizedString("evening", comment: nil))
                            self.AvailableDaysArray.add(record)
                            var justI = 1
                            self.AvailableAllDaysArray = NSMutableArray()
                            
                            for (_, element) in self.availabilityDict.enumerated() {
                                let result1 = self.theme.CheckNullValue((element as AnyObject).object(forKey: "day") as AnyObject)!
                                
                                let avaialbletime  : String = self.theme.CheckNullValue((((element as AnyObject).object(forKey: "hour"))! as AnyObject).object(forKey: "morning") as AnyObject)!
                                let avaialbleAftertime  : String =   self.theme.CheckNullValue((((element as AnyObject).object(forKey: "hour"))! as AnyObject).object(forKey: "afternoon") as AnyObject)!
                                let avaialbleevetime  : String = self.theme.CheckNullValue((((element as AnyObject).object(forKey: "hour"))! as AnyObject).object(forKey: "evening") as AnyObject)!
                                let record  = AvailableRecord (dayrec: result1,mornigrec: avaialbletime ,Afterrec: avaialbleAftertime,eveningrec: avaialbleevetime)
                                self.AvailableDaysArray.add(record)
                                
                            }
                            
                            for i in 0..<7
                            {
                                let availableObj = self.AvailableDaysArray.object(at: justI) as! AvailableRecord
                                if availableObj.AvailDays as String == self.weekFullDay[i]{
                                    let record  = AvailableRecord (dayrec: availableObj.AvailDays as String,mornigrec: availableObj.AvailMornigtime as String,Afterrec: availableObj.AvailAftertime as String,eveningrec: availableObj.Availeveningtime as String)
                                    self.AvailableAllDaysArray.add(record)
                                    if  justI < self.AvailableDaysArray.count-1{
                                        justI += 1
                                    }
                                }else{
                                    let record  = AvailableRecord (dayrec: self.weekFullDay[justI],mornigrec: "0" ,Afterrec: "0",eveningrec: "0")
                                    self.AvailableAllDaysArray.add(record)
                                }
                            }
                            
                            
                            
                            
                        }else{
                            //self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                        }
                        
                        
                        self.myProfileTblView.reloadData()
                        self.availabletable.reloadData()
                        
                        
                        
                        self.loadProfileTblView()
                        
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                }
            }
            
        }
    }
    func GetReviews(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["user_id":"\(objUserRecs.providerId)",
                                 "role":"tasker",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]
        // print(Param)
        
        url_handler.makeCall(Get_ReviewsURl, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.reviewsTblView.isHidden=false
            self.reviewsTblView.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let Dict:NSDictionary=responseObject.object(forKey: "data") as! NSDictionary
                    let status=self.theme.CheckNullValue(Dict.object(forKey: "status") as AnyObject)! as NSString
                    
                    if(status == "1")
                    {
                        if(((Dict.object(forKey: "response") as AnyObject).object(forKey: "reviews")! as AnyObject).count>0){
                            let  listArr:NSArray=(Dict.object(forKey: "response") as AnyObject).object(forKey: "reviews") as! NSArray
                            if(self.nextPageStr==1){
                                self.reviewsArray.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerated() {
                                let rec = ReviewRecords(name: self.theme.CheckNullValue((element as AnyObject).object(forKey: "user_name") as AnyObject)!, time: self.theme.CheckNullValue((element as AnyObject).object(forKey: "date") as AnyObject)!, desc: self.theme.CheckNullValue((element as AnyObject).object(forKey: "comments") as AnyObject)!, rate: self.theme.CheckNullValue((element as AnyObject).object(forKey: "rating") as AnyObject)!, img: self.theme.CheckNullValue((element as AnyObject).object(forKey: "user_image") as AnyObject)!,ratting:self.theme.CheckNullValue((element as AnyObject).object(forKey: "image") as AnyObject)!,jobid :self.theme.CheckNullValue((element as AnyObject).object(forKey: "booking_id") as AnyObject)!)
                                
                                self.reviewsArray.add(rec)
                            }
                            self.reviewsTblView.reloadData()
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:self.theme.setLang("no_leads"), duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                            }
                        }
                    }
                    else
                    {
                        //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    func refreshNewLeads(){
        
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        reviewsTblView.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
            self.GetReviews()
            
            }, loadingView: loadingView)
        reviewsTblView.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        reviewsTblView.dg_setPullToRefreshBackgroundColor(reviewsTblView.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
            GetReviews()
        }
    }
    func UITableView_Auto_Height()
    {
        if(self.myProfileTblView.contentSize.height > self.myProfileTblView.frame.height){
            
        }
    }
    deinit {
        // reviewsTblView.dg_removePullToRefresh()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
