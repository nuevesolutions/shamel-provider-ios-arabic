//
//  AddCategoryViewController.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 04/10/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class AddCategoryViewController: RootBaseViewController,SelectCategoryDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var title_lbl: CustomLabelHeader!
     var URL_handler:URLhandler=URLhandler()
    
    @IBOutlet weak var Added_Category_list: UITableView!
    @IBOutlet weak var category_lbl: UILabel!
    @IBOutlet weak var AddCategory_btn: UIButton!
    var selectCat:SelectCategoryViewController = SelectCategoryViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        selectCat.delegate = self

        // Do any additional /Users/casperonios/Documents/SakthiVel/Registeration_page/HandyForAll Product/Maidac Partner  2 2/PlumberJJ/Modelsetup after loading the view.
    }

    @IBAction func backBtn_Action(_ sender: UIButton) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    
    @IBAction func add_category(_ sender: AnyObject) {
        let registerAdd_VC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        self.navigationController?.pushViewController(withFlip: registerAdd_VC, animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    func changeBackgroundColor(_ array: NSMutableArray) {
        Added_Category_list.reload()
    
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
                return jobDetail.categoryEditArray.count
    }
    func tableView(_ tableView: UITableView!, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
       
        let cell = Added_Category_list.dequeueReusableCell(withIdentifier: "cell") as! AddCategoryListTableViewCell
       let rec = jobDetail.categoryEditArray[indexPath.row] as! CategoryEditRecord
        cell.cat_lbl.text = rec.subCategory
        cell.delete_btn.tag = indexPath.row
        cell.delete_btn.addTarget(self, action: #selector(deleteSelected), for: UIControlEvents.touchUpInside)
                return cell
    }
    func deleteSelected(_ sender: UIButton!) {
        jobDetail.categoryEditArray.removeObject(at: sender.tag)
        registerData.category_details.removeObject(at: sender.tag)
        jobDetail.checkSubCategoryArray.removeObject(at: sender.tag)
        Added_Category_list.reload()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        Added_Category_list.reload()
        
    }
    @IBAction func didClick_Con(_ sender: AnyObject) {
        
        self.showProgress()
        let parameter = [
            "firstname" : registerData.firstname,
            "lastname" : registerData.lastname,
            "username" : registerData.username,
            "email" : registerData.emailAddress,
            "password" : registerData.password,
            "password_confirmation" : registerData.confrimPassword,
            "phone_code" : registerData.countryCode,
            "phone_number" : registerData.mobileNumber,
            "gender" : "",
            "birthdate" : registerData.dob,
            "address" : registerData.address,
            "line2" : registerData.house_no,
            "city" : registerData.city,
            "state" : registerData.state,
            "country" : registerData.country,
            "zipcode" : registerData.zipcode,
            "image": "",
            "availability_address" : registerData.workLocation,
            "location_lat" : registerData.lat,
            "location_lng" : registerData.long,
            "radius" : registerData.radius,
            "working_days" : registerData.workingDays,
            "profile_details" : registerData.profile_dtl,
            "taskerskills" : registerData.category_details
            ] as [String : Any]
        
        
        
        
        
        URL_handler.makeCall("\(reg_form2)", param: parameter as NSDictionary, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
                            if(error != nil){
                                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault as AnyObject, title: Appname)
                            } else {
                               if(responseObject != nil){
                               let dict:NSDictionary=responseObject!
                                                        var status = self.theme.CheckNullValue(dict.object(forKey: "status"))!
                                                        if(status == "1")
                                                        {
                                print("Sucessssss/........//.....")
                                                                                                                  }
                                                        else{
                                                            self.theme.AlertView("\(Appname)", Message: "\(self.theme.CheckNullValue(dict.object(forKey: "response"))!)", ButtonTitle: "OK")
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                }
                                                
                                            })
        
    }
   
}
