//
//  CountryCodeViewController.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 29/09/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class CountryCodeViewController: UIViewController,UITableViewDelegate,UISearchBarDelegate {
    @IBOutlet weak var countrySearchController: UISearchBar!
     @IBOutlet weak var Title_lbl: UILabel!
      @IBOutlet var Tbl_view: UITableView!
var searchArray = [String]()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func bactBtn_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }

    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(countrySearchController.text != ""){
            return searchArray.count
        }else{
            return theme.codename.count;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = Tbl_view.dequeueReusableCell(withIdentifier: "cell") as! Country_CodeTableViewCell
        cell.textLabel?.text = ""
        //  var font = UIFont.init(name: plumberMediumFont, size: <#T##CGFloat#>)
        cell.textLabel?.font = PlumberMediumFont
        cell.textLabel?.attributedText = NSAttributedString(string: "")
        
        if(countrySearchController.text != ""){
            cell.configureCellWith(searchTerm:countrySearchController.text!, cellText: searchArray[indexPath.row])
            return cell
        }else{
            cell.textLabel?.text! = theme.codename[indexPath.row] as! String
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if(searchArray.count == 0){
            countrySearchController.text = theme.codename[indexPath.row] as? String
            let index = theme.codename.index(of: countrySearchController.text!)
            signup.selectedCode = theme.code[index] as! String 
            
        }else{
            countrySearchController.text = searchArray[indexPath.row]
            let index = theme.codename.index(of: countrySearchController.text!)
            signup.selectedCode = theme.code[index] as! String
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        searchArray.removeAll(keepingCapacity: false)
        
        
        let NewText = (searchBar.text! as NSString).replacingCharacters(in: range, with:text)
        print(NewText);
        let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
        var searchString = String()
        (NewText as String).enumerateSubstrings(in: range, options: .byComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
            searchString.append(substring!)
            searchString.append("*")
        })
        let searchPredicate = NSPredicate(format: "SELF LIKE[c] %@", searchString)
        let array = (theme.codename as NSArray).filtered(using: searchPredicate)
        searchArray = array as! [String]
        Tbl_view.reloadTable()
        return true
        
    }

}
