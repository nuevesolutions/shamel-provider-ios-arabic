//
//  RegisterAddressViewController.swift
//  PlumberJJ
//
//  Created by casperon_macmini on 18/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit
import WDImagePicker
class RegisterAddressViewController: RootBaseViewController,UITextFieldDelegate,UIActionSheetDelegate, WDImagePickerDelegate {
    @IBOutlet var scroll_View: UIScrollView!
    @IBOutlet var pincode_View: UIView!
    @IBOutlet var city_View: UIView!
    @IBOutlet var area_View: UIView!
    @IBOutlet var streetAddr_View: UIView!
    @IBOutlet var dateTxt_Fld: UITextField!
    @IBOutlet var street_Txt: UITextField!
    @IBOutlet weak var street_txt_line2: UITextField!
    
    @IBOutlet weak var state_view: UIView!
    @IBOutlet weak var streetAdress2line_view: UIView!
    @IBOutlet var selectAge_View: UIView!
    @IBOutlet var state_Txt: UITextField!
    @IBOutlet var city_Txt: UITextField!
  
    @IBOutlet var country_Txt: UITextField!
  //  @IBOutlet var date_Picker:UIDatePicker!
    @IBOutlet var selectImg_Btn: UIButton!
    @IBOutlet var profile_Img: UIImageView!
    @IBOutlet var pincode_Txt: UITextField!
    @IBOutlet var continue_Btn: CustomButton!
     @IBOutlet var Placesearch_tableview: UITableView!
    
    var date_Picker:UIDatePicker!
    
    var imagePicker = WDImagePicker()
    var imagedata : Data = Data()
    
    
    struct Place {
        let id: String
        let description: String
    }

    var places = [Place]()
    var getTextfieldtag : NSString = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    var Addaddress_Data : Addaddress = Addaddress()
    
    func pickUpDate(_ textField : UITextField){
        
        // DatePicker
        self.date_Picker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.date_Picker.backgroundColor = UIColor.white
        self.date_Picker.datePickerMode = UIDatePickerMode.date
        textField.inputView = self.date_Picker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(RegisterAddressViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace , target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain
            , target: self, action: #selector(RegisterAddressViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        }
    
    func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateTxt_Fld.text = dateFormatter1.string(from: date_Picker.date)
        dateTxt_Fld.resignFirstResponder()
    }
    
    func cancelClick() {
         self.dateTxt_Fld.resignFirstResponder()
    }
    
    func setView(){
        
        Placesearch_tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        street_Txt.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)

        self.Placesearch_tableview.isHidden=true
        scroll_View.contentSize = CGSize(width: UIScreen.main.bounds.width,height: profile_Img.frame.maxY+continue_Btn.frame.size.height+10)
        
        pincode_View.layer.shadowColor = UIColor.lightGray.cgColor
        state_view.layer.shadowColor = UIColor.lightGray.cgColor
        state_view.layer.shadowOffset = CGSize(width: 5, height: 2)
        state_view.layer.shadowOpacity = 0.8
        state_view.layer.shadowRadius = 1.0
       streetAdress2line_view.layer.shadowColor = UIColor.lightGray.cgColor
        streetAdress2line_view.layer.shadowOffset = CGSize(width: 5, height: 2)
        streetAdress2line_view.layer.shadowOpacity = 0.8
        streetAdress2line_view.layer.shadowRadius = 1.0

        pincode_View.layer.shadowOffset = CGSize(width: 5, height: 2)
        pincode_View.layer.shadowOpacity = 0.8
        pincode_View.layer.shadowRadius = 1.0
        
        selectAge_View.layer.shadowColor = UIColor.lightGray.cgColor
        selectAge_View.layer.shadowOffset = CGSize(width: 5, height: 2)
        selectAge_View.layer.shadowOpacity = 0.8
        selectAge_View.layer.shadowRadius = 1.0
        
        streetAddr_View.layer.shadowColor = UIColor.lightGray.cgColor
        streetAddr_View.layer.shadowOffset = CGSize(width: 5, height: 2)
        streetAddr_View.layer.shadowOpacity = 0.8
        streetAddr_View.layer.shadowRadius = 1.0
        
        city_View.layer.shadowColor = UIColor.lightGray.cgColor
        city_View.layer.shadowOffset = CGSize(width: 5, height: 2)
        city_View.layer.shadowOpacity = 0.8
        city_View.layer.shadowRadius = 1.0
        
        area_View.layer.shadowColor = UIColor.lightGray.cgColor
        area_View.layer.shadowOffset = CGSize(width: 5, height: 2)
        area_View.layer.shadowOpacity = 0.8
        area_View.layer.shadowRadius = 1.0
        
        selectImg_Btn.layer.shadowColor = UIColor.lightGray.cgColor
        selectImg_Btn.layer.shadowOffset = CGSize(width: 1, height: 1)
        selectImg_Btn.layer.shadowOpacity = 0.8
        selectImg_Btn.layer.shadowRadius = 1.0

        profile_Img.layer.cornerRadius = profile_Img.frame.width/2
        profile_Img.clipsToBounds = true
        
        self.dateTxt_Fld.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterAddressViewController.selectDate_Action(_:))))

    }
    
    
    func getPlaces(_ searchString: String) {
        
        let request = requestForSearch(searchString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            
            self.handleResponse(data, response: response as? HTTPURLResponse, error: error as! NSError)
            
        }) 
        
        task.resume()
        
    }
    
    func requestForSearch(_ searchString: String) -> URLRequest {
        
        
        
        let params = [
            
            "input": searchString,
            
            // "type": "(\(placeType.description))",
            
            //"type": "",
            
            "key": "\(googleApiKey)"
            
        ]
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")
        
        
        
        return NSMutableURLRequest(
            
            url: URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")!
            
            
        ) as URLRequest
        
    }
    
    func query(_ parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sorted(by: <)) {
            let value: AnyObject! = parameters[key]
            
            components += [(escape(key), escape("\(value!)"))]
        }
        
        return components.map{"\($0)=\($1)"}.joined(separator: "&")
    }
    
    
    func escape(_ string: String) -> String {
        
        let legalURLCharactersToBeEscaped: CFString = ":/?&=;+!@#$()',*" as CFString
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string as CFString, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        
    }
    


    func handleResponse(_ data: Data!, response: HTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            
            return
            
        }
        
        
        
        if response == nil {
            
            print("GooglePlacesAutocomplete Error: No response from API")
            
            return
            
        }
        
        
        
        if response.statusCode != 200 {
            
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            
            return
            
        }
        
        
        do {
            
            let json: NSDictionary = try JSONSerialization.jsonObject(
                
                with: data,
                
                options: JSONSerialization.ReadingOptions.mutableContainers
                
                ) as! NSDictionary
            
            DispatchQueue.main.async(execute: {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                
                
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        
                        return Place(
                            
                            id: prediction["id"] as! String,
                            
                            description: prediction["description"] as! String
                            
                        )
                        
                    }
                    self.Placesearch_tableview.reload()
                   // self.Location_tableview.reload()
                    
                    
                }
                
            })
        }
            
            
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
        
        
        // Perform table updates on UI thread
        
        
        
    }
    
    func selectDate_Action(_ val:String){
        
        
    }
    
    @IBAction func continueBtn_Action(_ sender: CustomButton) {
        
        let selelctTime_VC = storyboard?.instantiateViewController(withIdentifier: "Slot_VCID") as! AddSlotsViewController
        self.navigationController?.pushViewController(withFlip: selelctTime_VC, animated: true)
        
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateTxt_Fld{
        self.pickUpDate(self.dateTxt_Fld)
        }
    }
    
    func TextfieldDidChange(_ textField:UITextField)
    {
        if(textField == street_Txt)
        {
            if(street_Txt.text != "")
            {
                if(places.count == 0)
                {
                    self.Placesearch_tableview.isHidden=true
                    
                }
                    
                else
                {
                    self.Placesearch_tableview.isHidden=false
                    
                }
                
                getTextfieldtag = "1"
                getPlaces(street_Txt.text!)
                
                
                
            }
            else
            {
                
                places.removeAll()
                self.Placesearch_tableview.isHidden=true
            }
        }
    }

    func tableView(_ tableView: UITableView!, heightForRowAtIndexPath indexPath: IndexPath!) -> CGFloat
    {
        return 40.0
    }
    func tableView(_ tableView: UITableView!, numberOfRowsInSection section: Int) -> Int{
        
        
        return places.count
   
    }
    func tableView(_ tableView: UITableView!, cellForRowAtIndexPath indexPath: IndexPath!) -> UITableViewCell{
        
        let cell = UITableViewCell()
        if tableView == Placesearch_tableview{
            
            let Placecell = (tableView.dequeueReusableCell(withIdentifier: "cell") )!
            let place = self.places[indexPath.row]
            Placecell.textLabel?.textColor = UIColor.black
            Placecell.textLabel?.text=place.description
            return Placecell
            
        }
         return cell
    }
    
    func tableView(_ tableView: UITableView!, didSelectRowAtIndexPath indexPath: IndexPath!)
    {
        if tableView .isEqual(Placesearch_tableview)
        {
            
            if  getTextfieldtag == "1"
                
            {
                let place = self.places[indexPath.row]
                
                self.showProgress()
                Addaddress_Data.YourLocality = "\(place.description)" as NSString
                
                let geocoder: CLGeocoder = CLGeocoder()
                
                geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) in
                    self.DismissProgress()
                    if (error != nil) {
                        print("Error \(error!)")
                    } else if let placemark = placemarks?[0] {
                        
                        
                        print("the responsadsase is .......\(placemark)")
                        
                        //                let CityName:String?=placemark.subAdministrativeArea
                        let Locality:String?=placemark.subLocality
                        
                        let ZipCode:String?=placemark.postalCode
                        
                        
                        if (placemark.subThoroughfare != nil)
                        {
                            self.street_txt_line2.text = self.theme.CheckNullValue(placemark.subThoroughfare! as AnyObject)
                            
                        }
                        if (placemark.locality != nil)
                        {
                            self.city_Txt.text = self.theme.CheckNullValue(placemark.locality! as AnyObject)
                        }
                        if (placemark.administrativeArea != nil)
                        {
                            self.state_Txt.text = self.theme.CheckNullValue(placemark.administrativeArea! as AnyObject)
                        }
                        if (placemark.country != nil)
                        {
                            self.country_Txt.text = self.theme.CheckNullValue(placemark.country! as AnyObject)
                        }

//                        if(placemark.country != nil){
//                            self.countryTxtField.text = placemark.country!
//                        }
                        
//                        if (placemark.subAdministrativeArea != nil)
//                        {
//                            self.city_Txt.text = self.theme.CheckNullValue(placemark.subAdministrativeArea!)
//                        }
                        
                        
                        
                        if(ZipCode != nil)
                        {
                            self.pincode_Txt.text=""
                            self.pincode_Txt.text = "\(placemark.country!) - \(ZipCode!)"
                           // self.postalCodeTxtField.text=ZipCode!
                            
                        }
//                        else
//                        {
//                            if(placemark.country != nil){
//                                   // self.countryTxtField.text = placemark.country!
//                                self.pincode_Txt.text = placemark.country!
//                            }

//                        }
                        
                    }
                    })
                
                
                
                url_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(googleApiKey)&address=\(place.description.addingPercentEscapes(using: String.Encoding.utf8)!)" as NSString) { (responseObject) -> () in
                    
                    
                     if(responseObject != nil)
                    {
                        let responseObject = responseObject!
                        
                        
                        let results:NSArray=(responseObject.object(forKey: "results"))! as! NSArray
                        if results.count > 0
                        {
                            let firstItem: NSDictionary = results.object(at: 0) as! NSDictionary
                            let geometry: NSDictionary = firstItem.object(forKey: "geometry") as! NSDictionary
                            let locationDict:NSDictionary = geometry.object(forKey: "location") as! NSDictionary
                            let lat:NSNumber = locationDict.object(forKey: "lat") as! NSNumber
                            let lng:NSNumber = locationDict.object(forKey: "lng") as! NSNumber
                            
                            
                            self.Addaddress_Data.Latitude="\(lat)" as NSString
                            
                            self.Addaddress_Data.Longitude="\(lng)" as NSString
                        }
                        
                        
                    }
                    else
                    {
                        self.Addaddress_Data.Latitude="0"
                        self.Addaddress_Data.Longitude="0"
                        
                    }
                    
                    
                    
                }
                
                street_Txt.text=Addaddress_Data.YourLocality as String
                Placesearch_tableview.isHidden=true
            }
        }
    }
    
       @IBAction func backBtn_Action(_ sender: AnyObject) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
   
    @IBAction func selectImg_Action(_ sender: UIButton){
        
        let ImagePicker_Sheet = UIAlertController(title: nil, message: theme.setLang("select_image"), preferredStyle: .actionSheet)
        
        let Camera_Picker = UIAlertAction(title: theme.setLang("camera"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.Camera_Pick()
        })
        let Gallery_Picker = UIAlertAction(title: theme.setLang("gallery") , style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //
            self.Gallery_Pick()
            
        })
        
        let cancelAction = UIAlertAction(title: theme.setLang("cancel"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        ImagePicker_Sheet.addAction(Camera_Picker)
        ImagePicker_Sheet.addAction(Gallery_Picker)
        ImagePicker_Sheet.addAction(cancelAction)
        
        self.present(ImagePicker_Sheet, animated: true, completion: nil)
         }
    
    
    func Camera_Pick()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            self.imagePicker = WDImagePicker()
            self.imagePicker.cropSize = CGSize(width: 280, height: 280)
            self.imagePicker.delegate = self
            self.imagePicker.resizableCropArea = true
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker.imagePickerController, animated: true, completion: nil)
        }
            
        else
        {
            Gallery_Pick()
        }
    }
    
    func Gallery_Pick()
    {
        
        self.imagePicker = WDImagePicker()
        self.imagePicker.cropSize = CGSize(width: 280, height: 280)
        self.imagePicker.delegate = self
        self.imagePicker.resizableCropArea = true
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker.imagePickerController, animated: true, completion: nil)
        
    }
    
    
    func imagePicker(_ imagePicker: WDImagePicker, pickedImage: UIImage) {
        let image = UIImage.init(cgImage: pickedImage.cgImage!, scale: 0.25 , orientation:.up)
        imagedata = UIImageJPEGRepresentation(image, 0.1)!;
        self.profile_Img.image = image
        self.hideImagePicker()
      //  self.uploadImageAndData()
    }
    
    func hideImagePicker() {
        self.imagePicker.imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setView()
    }

    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
