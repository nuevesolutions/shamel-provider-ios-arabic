//
//  InitialViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/18/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var _pageViewController: UIPageViewController?
    var _pageTitles = ["Welcome", "Welcome", "Welcome", "Welcome"]
    let _pageImages = ["background", "background", "background", "background"]
    let _descImages = ["Plumb1", "Plumb2", "Plumb3", "Plumb4"]
    var _descMsgs = ["Our rich & responsive interface will give you the best user experience.", "Customers are looking to hire experienced professionals just like you !!", "Connect with customers to get hired and earn more money !!!", "Carry your ToolKit and help the people around the world !!!"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.transtion()
       
        
        _pageTitles = [Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil)]
        loginBtn.setTitle(Language_handler.VJLocalizedString("sign_in", comment: nil), for: UIControlState())
        registerBtn.setTitle(Language_handler.VJLocalizedString("register", comment: nil), for: UIControlState())
        _descMsgs = [Language_handler.VJLocalizedString("desc_msgs1", comment: nil), Language_handler.VJLocalizedString("desc_msgs2", comment: nil), Language_handler.VJLocalizedString("desc_msgs3", comment: nil), Language_handler.VJLocalizedString("desc_msgs4", comment: nil)]
        loadSplashVideo()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickLoginBtn(_ sender: AnyObject) {
        
        
    }
    @IBAction func didClickRegisterBtn(_ sender: AnyObject) {
        
    }
    func loadSplashVideo(){
        
        self.view.bringSubview(toFront: registerBtn)
        self.view.bringSubview(toFront: loginBtn)
        
        //        registerBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        //        registerBtn.layer.cornerRadius=5;
        //        registerBtn.layer.shadowOpacity = 0.4
        //        registerBtn.layer.shadowRadius = 4
        //
        //        loginBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        //        loginBtn.layer.cornerRadius=5;
        //        loginBtn.layer.shadowOpacity = 0.4
        //        loginBtn.layer.shadowRadius = 4
        
        _pageViewController = storyboard!.instantiateViewController(withIdentifier: "WalkthroughPageView") as? UIPageViewController
        _pageViewController!.dataSource = self
        _pageViewController!.delegate = self
        
        let startingViewController = viewControllerAtIndex(0)
        let viewControllers = [startingViewController!]
        _pageViewController!.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        
        // Change the size of page view controller
        _pageViewController!.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 60)
        
        addChildViewController(_pageViewController!)
        view.addSubview(_pageViewController!.view)
        _pageViewController!.didMove(toParentViewController: self)
        
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed) {
            let pageContentViewController = pageViewController.viewControllers![0] as! WalkthroughPageContentViewController
            let index = pageContentViewController.pageIndex
            let backgroundImageName = _pageImages[index] as NSString
            
            UIView.transition(with: self.backgroundImage, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve,
                                      animations: { () -> Void in
                                        self.backgroundImage.image = UIImage(named: backgroundImageName as String)
                }, completion: { (Bool) -> Void in
                    
                }
            )
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContentViewController = viewController as! WalkthroughPageContentViewController
        var index = pageContentViewController.pageIndex;
        
        if ((index == 0) || (index == NSNotFound)) {
            return nil;
        }
        
        index -= 1;
        return self.viewControllerAtIndex(index);
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let pageContentViewController = viewController as! WalkthroughPageContentViewController
        var index = pageContentViewController.pageIndex;
        
        if (index == NSNotFound) {
            return nil
        }
        
        index += 1;
        if (index == _pageTitles.count) {
            return nil
        }
        
        return viewControllerAtIndex(index);
    }
    
    func viewControllerAtIndex(_ index: NSInteger) -> UIViewController? {
        if ((_pageTitles.count == 0) || (index >= _pageTitles.count)) {
            return nil
        }
        
        let pageContentViewController = storyboard!.instantiateViewController(withIdentifier: "WalkthroughPageContent") as! WalkthroughPageContentViewController
        pageContentViewController.titleText = _pageTitles[index] as NSString
        pageContentViewController.descText = _descMsgs[index] as NSString
        pageContentViewController.imgText = _descImages[index] as NSString
        pageContentViewController.pageIndex = index
        
        return pageContentViewController
    }
    
    
    func presentationCount(for pageViewController: UIPageViewController) -> NSInteger {
        return _pageTitles.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> NSInteger {
        return 0
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   func transtion()
   {
    
    
    var frame = self.registerBtn!.frame
    frame.origin.y = frame.origin.y + 200
    self.registerBtn?.frame = frame
    var frame1 = self.loginBtn!.frame
    frame1.origin.y = frame1.origin.y + 200
    self.loginBtn?.frame = frame1
    
    UIView.animate(withDuration: 1 , delay:0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
        
        var frame = self.registerBtn!.frame
        frame.origin.y = frame.origin.y - 200
        self.registerBtn?.frame = frame
        
        var frame1 = self.loginBtn!.frame
        frame1.origin.y = frame1.origin.y - 200
        self.loginBtn?.frame = frame1
        
    }, completion: nil)
    
    
    
    
    
    }
    
    
    
}
