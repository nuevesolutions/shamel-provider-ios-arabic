//
//  ExtraFieldsCell.swift
//  Plumbal
//
//  Created by Nueve on 26/04/18.
//  Copyright © 2018 Casperon Tech. All rights reserved.
//

import UIKit

class ExtraFieldsCell: UITableViewCell {

    @IBOutlet weak var extraField: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
